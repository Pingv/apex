# APEX

## Versions

- The version 1.0 is the version published with the paper **APEX (Aqueous Photochemistry of Environmentally occurring Xenobiotics): a free software tool to predict the kinetics of photochemical processes in surface waters** (2014), by Marco BODRATO and Davide VIONE, DOI: [10.1039/c3em00541k](https://doi.org/10.1039/c3em00541k), and can be found in the Supplementary files of the original publication at [rsc.org](https://pubs.rsc.org/en/content/articlelanding/2014/EM/C3EM00541K)
- The version 1.1 is an update, for the published paper **A Critical View of the Application of the APEX Software (Aqueous Photochemistry of Environmentally-Occurring Xenobiotics) to Predict Photoreaction Kinetics in Surface Freshwaters** (2019) by Davide VIONE.

Further updates will be published here as an open project.

## Project

To be done...
