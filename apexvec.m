% Vectorized version of the APEX program.

% Copyright 2011, 2012 by Davide Vione and Marco Bodrato
% Licence CC BY-NC-SA

% This is part of the APEX program
% Aqueous Photochemistry of Environmentally occurring Xenobiotics

function v = apexvec (file_prefix, d, CNO3, CNO2, NPOC, CCO3, CHCO3, CBr, kP_OH, kP_CO3, kP_DOM, kP_1O2, fi_P, y_OH, y_CO3, y_1O2, y_3DOM, y_Phot, qyieldOH_CDOM, carbonateyieldCO3_CDOM, qyield1O2_CDOM, qyieldTriplet_CDOM)
	[t_OH, t_CO3, t_1O2, t_3DOM, t_Phot, t_tot, k_OH, k_CO3, k_1O2, k_3DOM, k_Phot, k_tot, coOH, coCO3, co1O2, co3DOM, f_OH, f_CO3, f_1O2, f_3DOM, f_Phot, f_tot, y_tot, role_OH_P, role_CO3_P, role_1O2_P, role_3DOM_P, role_Phot_P, role_OH_I, role_CO3_I, role_1O2_I, role_3DOM_I, role_Phot_I, NO3_OH, NO2_OH, DOM_OH] = apex (file_prefix, d, CNO3, CNO2, NPOC, CCO3, CHCO3, CBr, kP_OH, kP_CO3, kP_DOM, kP_1O2, fi_P, y_OH, y_CO3, y_1O2, y_3DOM, y_Phot, qyieldOH_CDOM, carbonateyieldCO3_CDOM, qyield1O2_CDOM, qyieldTriplet_CDOM);
	v = [t_OH, t_CO3, t_1O2, t_3DOM, t_Phot, t_tot, k_OH, k_CO3, k_1O2, k_3DOM, k_Phot, k_tot, coOH, coCO3, co1O2, co3DOM, f_OH, f_CO3, f_1O2, f_3DOM, f_Phot, f_tot, y_tot, role_OH_P, role_CO3_P, role_1O2_P, role_3DOM_P, role_Phot_P, role_OH_I, role_CO3_I, role_1O2_I, role_3DOM_I, role_Phot_I, NO3_OH, NO2_OH, DOM_OH];
endfunction
