% Main computation function for the APEX program.

% Copyright 2011, 2012 by Davide Vione and Marco Bodrato
% Licence CC BY-NC-SA

% This is part of the APEX program
% Aqueous Photochemistry of Environmentally occurring Xenobiotics

function [t_OH, t_CO3, t_1O2, t_3DOM, t_Phot, t_tot, k_OH, k_CO3, k_1O2, k_3DOM, k_Phot, k_tot, coOH, coCO3, co1O2, co3DOM, f_OH, f_CO3, f_1O2, f_3DOM, f_Phot, f_tot, y_tot, role_OH_P, role_CO3_P, role_1O2_P, role_3DOM_P, role_Phot_P, role_OH_I, role_CO3_I, role_1O2_I, role_3DOM_I, role_Phot_I, NO3_OH, NO2_OH, DOM_OH] = apex (file_prefix, d, CNO3, CNO2, NPOC, CCO3, CHCO3, CBr, kP_OH, kP_CO3, kP_DOM, kP_1O2, fi_P, y_OH, y_CO3, y_1O2, y_3DOM, y_Phot, qyieldOH_CDOM, carbonateyieldCO3_CDOM, qyield1O2_CDOM, qyieldTriplet_CDOM)

	IC = CCO3 + CHCO3;
	CP = 1e-8;
	V = 0.1*d;
%===========================================================================================================================
%  Enable the parts below (i.e., delete "%" overall) if you want to model water evaporation (do not forget to do the same at the bottom of this function as well).
%  The value of "d0" below is the initial depth of the lake, before evaporation. 10 m is a default value, modify it if necessary (note: it should be the same value of the maximum depth entered in Plotgraph or Savetable). 
%  Retype "%" in order to restore the default type of modelling (no evaporation).


	%  CNO3o = CNO3;
	%  CNO2o = CNO2;
	%  NPOCo = NPOC;
	%  CCO3o = CCO3;
	%  CHCO3o = CHCO3;
	%  Vo = V;

	%  d0=10;

	%  CNO3=(CNO3)*(d0/d)^3;
	%  CNO2=(CNO2)*(d0/d)^3;
	%  CHCO3=(CHCO3)*(d0/d)^3;
	%  CCO3=(CCO3)*(d0/d)^3;
	%  NPOC=NPOC*(d0/d)^3;
	%  V = V/((d0/d)^3);

% (do not delete this "%") The first step of selection/deselection for evaporation ends here, but please go to the end of the file and do the same for the second step
%===========================================================================================================================

	ALL = csvread(strcat(file_prefix,".csv"),1,0);
	LL = ALL(:,1);
	ENO3 = ALL(:,2);
	ENO2 = ALL(:,3);
	phi_NO2_OH = ALL(:,4);
	p0sun = ALL(:,5);
	phi = ALL(:,6);
	EP = ALL(:,7);
	Aw = ALL(:,8);

	RL = 1:length(LL);
	paTOT = RL;
	paDOM = RL;
	paNO3 = RL;
	paNO2 = RL;
	RNO2 = RL;
	paP = RL;

	for I = 1:length(LL),
	  ANO3 = ENO3(I)*d*CNO3*100;
	  ANO2 = ENO2(I)*d*CNO2*100;
	  if (Aw(I) > 0)
	    ADOM = Aw(I)*d*100;
	  else
	%   ADOM = NPOC*0.674*exp(-0.013*LL(I))*d*100;
            ADOM = NPOC*0.45*exp(-0.015*LL(I))*d*100;
	  endif
          if (phi(I) < 0)
            phi(I) = fi_P;
          endif
	  if (EP(I) > 0)
	    AP = EP(I)*d*CP*100;
	  else
	    AP = 0;
	  endif

	  ATOT = ANO3+ANO2+ADOM+AP;
	  paTOT(I) = p0sun(I)*(1-exp(-log(10)*ATOT));
	  paDOM(I) = paTOT(I)*ADOM/ATOT;
	  paNO3(I) = paTOT(I)*ANO3/ATOT;
	  paNO2(I) = paTOT(I)*ANO2/ATOT;
	  paP(I)   = paTOT(I)*  AP/ATOT;
	  RL(I)    = paP(I)*phi(I);
	  RNO2(I)  = paNO2(I)*phi_NO2_OH(I);
	endfor;

	Pa_NO3 = integral(LL, paNO3);
	Pa_NO2 = integral(LL, paNO2);
	Pa_DOM = integral(LL, paDOM);
	Pa_TOT = integral(LL, paTOT);
	RPhot  = integral(LL, RL);

	ROH_NO3 = Pa_NO3*(4.3e-2)*(IC+0.0075)/(2.25*IC+0.0075);
	ROH_NO2 = integral(LL, RNO2);
        ROH_DOM = Pa_DOM*qyieldOH_CDOM;
	ROH_TOT = ROH_NO3+ROH_NO2+ROH_DOM;
	Scav = (5.0e4)*NPOC+(8.5e6)*CHCO3+(3.9e8)*CCO3+(1.1e10)*CBr+(1e10)*CNO2;
	if (kP_OH > 0)
	  t_OH = (0.1897e-5)*d*Scav/(ROH_TOT*kP_OH);
	  k_OH = log(2)/t_OH;
	else
	  k_OH = 0;
	  t_OH = Inf;
	endif;

	RCO3NO3 = ROH_NO3*11.3*IC/(IC+0.061);
	RCO3 = RCO3NO3+carbonateyieldCO3_CDOM*CCO3*Pa_DOM+ROH_TOT*((8.5e6)*CHCO3+(3.9e8)*CCO3)/Scav;

	if (kP_CO3 > 0)
	  t_CO3 = 0.000190*d*NPOC/(RCO3*kP_CO3);
	  k_CO3 = log(2)/t_CO3;
	else
	  k_CO3 = 0;
	  t_CO3 = Inf;
	endif;

	R3DOM = Pa_DOM*qyieldTriplet_CDOM;
	if (kP_DOM > 0)
	  kk = kP_DOM/(1+(NPOC/100000));
	  t_3DOM = (0.1929e-5)*d*(5e5+1700*NPOC)/(R3DOM*kP_DOM);
	  k_3DOM = log(2)/t_3DOM;
	else
	  k_3DOM = 0;
	  t_3DOM = Inf;
	endif;
	
	R1O2 = Pa_DOM*qyield1O2_CDOM;
	if (kP_1O2 > 0)
	  t_1O2 = (0.1929e-5)*d*(2.5e5)/(R1O2*kP_1O2);
	  k_1O2 = log(2)/t_1O2;
	else
	  k_1O2 = 0;
	  t_1O2 = Inf;
	endif;

	if (fi_P > 0)
	  t_Phot = log(2)*V*CP/(36000*RPhot);
	  k_Phot = (36000*RPhot)/(V*CP);
	else
	  k_Phot = 0;
	  t_Phot = Inf;
	endif;

	k_tot = k_OH+k_CO3+k_3DOM+k_1O2+k_Phot;
	t_tot = log(2)/k_tot;

	coOH = ROH_TOT/(V*Scav);
	coCO3 = RCO3/(V*100*NPOC);
	co1O2 = R1O2/(V*2.5e5);
	co3DOM = R3DOM/(V*5e5);

	f_OH = y_OH * k_OH;
	f_CO3 = y_CO3 * k_CO3;
	f_1O2 = y_1O2 * k_1O2;
	f_3DOM = y_3DOM * k_3DOM;
	f_Phot = y_Phot * k_Phot;
	f_tot = f_OH + f_CO3 + f_1O2 + f_3DOM + f_Phot;

	y_tot = f_tot/k_tot;
	role_OH_P = k_OH/k_tot;
	role_CO3_P = k_CO3/k_tot;
	role_1O2_P = k_1O2/k_tot;
	role_3DOM_P = k_3DOM/k_tot;
	role_Phot_P = k_Phot/k_tot;

	role_OH_I = f_OH/f_tot;
	role_CO3_I = f_CO3/f_tot;
	role_1O2_I = f_1O2/f_tot;
	role_3DOM_I = f_3DOM/f_tot;
	role_Phot_I = f_Phot/f_tot;

	NO3_OH = ROH_NO3/ROH_TOT;
	NO2_OH = ROH_NO2/ROH_TOT;
	DOM_OH = ROH_DOM/ROH_TOT;

%===========================================================================================================================
%  Enable the parts below (i.e., delete "%") if you want to model water evaporation. Retype "%" in order to restore the default type of modelling (no evaporation).

     %   CNO3 = CNO3o;
     %   CNO2 = CNO2o;
     %   NPOC = NPOCo;
     %   CCO3 = CCO3o;
     %   CHCO3 = CHCO3o;

% (do not delete this "%") Selection/deselection for evaporation ends here.
%===========================================================================================================================

endfunction
