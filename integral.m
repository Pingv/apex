% Function integral for the APEX program.

% Copyright 2011 by Davide Vione and Marco Bodrato
% Licence CC BY-NC-SA

% This is part of the APEX program
% Aqueous Photochemistry of Environmentally occurring Xenobiotics

function Area = integral (X,Y)
	Area = 0;
	for I = 2:length(X),
	  Area = Area + (Y(I)+Y(I-1))*(X(I)-X(I-1))/2;
	endfor;
endfunction;
