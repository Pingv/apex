% Function to plot a graph for the APEX program.

% Copyright 2011, 2012 by Davide Vione and Marco Bodrato
% Licence CC BY-NC-SA

% This is part of the APEX program
% Aqueous Photochemistry of Environmentally occurring Xenobiotics

% To be edited to obtain different tables.
% If a parameter (filemane) is given in Octave, it is used as a file name to save the table
% in CSV format. The default filename "saved_apex_data.csv" is used otherwise.
% Examples: savetable("table.csv") creates a CSV file with the matrix of values,
% in the same folder where savetable.m is placed. Note that 'savetable("table.csv")' is to be
% entered at the Octave prompt.

% The function savetable allows a table with two variable parameters to be saved. 
% The variable parameters

function savetable (filename)
% ===============================================================================================
% *** BEGINNING OF RANGE INPUT ***
	
  x=3:3:3;  % Range for the first variable, A:B:C means from A to C with steps of B.
  y=5:5:5;    % Range for the second variable.
	
% *** END OF RANGE INPUT ***
% ================================================================================================


% ================================================================================================
% *** INPUT FILE PREFIX ***
  % Here you should insert the name of the input file, which reports the spectra of the compound, 
  % sunlight and water, as well as the photolysis quantum yield. NOTE: the input file should be a .csv one.
  % file_prefix = prefix for filenames, named <file_prefix>"_LL.csv" ...

  file_prefix = "Test";
	
% *** END OF INPUT FILE PREFIX ***
% ================================================================================================

		
% ================================================================================================
% *** BEGINNING OF DATA INPUT ***
% NOTE: "-1" denotes the x variable, "-2" the y variable (see above for their ranges)

  % d = Water column depth (meters)
  d = -1;

  % CNO3 = Concentration of NO3- (molarity)
  CNO3 = 1e-10;

  % CNO2 = Concentration of NO2- (molarity)
  CNO2 = 1e-10;

  % NPOC = Dissolved Organic Carbon (DOC or NPOC, mgC/L or ppmC)
  NPOC = -2;

  % CCO3 = Concentration of CO3 2- (molarity)
  CCO3 = 1e-6;

  % CHCO3 = Concentration of HCO3- (molarity)
  CHCO3 = 1e-4;

  % CBr = Concentration of Br- (molarity)
  CBr = 1e-10;

  % kP_OH = Reaction rate between the substrate P and �OH (units of molarity^-1 seconds^-1, 0 if not available)
  kP_OH = 1.0e10;

  % kP_CO3 = Reaction rate between P and CO3-� (units of molarity^-1 seconds^-1, 0 if not available)
  kP_CO3 = 1;

  % kP_DOM = Reaction rate between P and CDOM triplet states, 3CDOM* (units of molarity^-1 seconds^-1, 0 if not available)
  kP_DOM = 4.5e7;

  % kP_1O2 = Reaction rate between P and singlet oxygen, 1O2 (units of molarity^-1 seconds^-1, 0 if not available)
  kP_1O2 = 6e4;

  % fi_P = direct photolysis quantum yield of P (unitless, 0 if not available) 
  fi_P = 0.33;

  % y_OH = yield of the intermediate via the �OH pathway (unitless, [formation rate of the intermediate via �OH]/[transformation rate of P])
  y_OH = 0.023;

  % y_CO3 = yield of the intermediate via the CO3-� pathway (unitless, [formation rate of the intermediate via CO3-�]/[transformation rate of P])
  y_CO3 = 0;

  % y_1O2 = yield of the intermediate via the 1O2 pathway (unitless, [formation rate of the intermediate via 1O2]/[transformation rate of P])
  y_1O2 = 0;

  % y_3DOM = yield of the intermediate via the 3CDOM* pathway (unitless, [formation rate of the intermediate via 3CDOM*]/[transformation rate of P])
  y_3DOM = 0.31;

  % y_Phot = yield of the intermediate via direct photolysis (unitless, [formation rate of the intermediate via direct photolysis]/[transformation rate of P])
  y_Phot = 0.25;

% *** END OF DATA INPUT ***
% ================================================================================================


% ------------------------------------------------------------------------------------------------
% === INPUT OF QUANTUM YIELD VALUES FOR REACTIVE SPECIES PHOTOPRODUCTION BY CDOM (ONLY FOR EXPERT USERS!!!) ===
% NOTE: MODIFY THESE VALUES ONLY IF YOU DEFINITELY KNOW WHAT YOU ARE DOING!!!!

  qyieldOH_CDOM = 3e-5;
  carbonateyieldCO3_CDOM = 6.5e-3;	% The relevant equation is: R_CO3_CDOM = carbonateyieldCO3_CDOM * CCO3 * PaCDOM
  qyield1O2_CDOM = 1.25e-3;
  qyieldTriplet_CDOM = 1.28e-3;

% === END OF INPUT OF QUANTUM YIELD VALUES
% ------------------------------------------------------------------------------------------------


%************************************************************************************************************
% ***** DO NOT TRY TO MODIFY THE FILE BELOW THIS POINT, UNLESS YOU ARE AN EXPERIENCED OCTAVE PROGRAMMER *****
%************************************************************************************************************


  ii = [d, CNO3, CNO2, NPOC, CCO3, CHCO3, CBr, kP_OH, kP_CO3, kP_DOM, kP_1O2, fi_P, y_OH, y_CO3, y_1O2, y_3DOM, y_Phot, qyieldOH_CDOM, carbonateyieldCO3_CDOM, qyield1O2_CDOM, qyieldTriplet_CDOM];
  ix = 0; iy = 0;
  for i = 1:length(ii),
    if (ii(i) == -1)
      ix = i;
    endif;
    if (ii(i) == -2)
      iy = i;
    endif;
  end;
  [xx,yy]=meshgrid(x,y);	% Prepare the grid for plotting.

  if (nargin() == 0)	% If the filename was given, save the CSV file.
    filename = "saved_apex_data.csv";
  endif;

  fid = fopen(filename, "w");
  fprintf (fid, '"X","Y","t_OH","t_CO3","t_1O2","t_3DOM","t_Phot","t_tot","k_OH","k_CO3","k_1O2","k_3DOM","k_Phot","k_tot","coOH","coCO3","co1O2","co3DOM","f_OH","f_CO3","f_1O2","f_3DOM","f_Phot","f_tot","y_tot","role_OH_P","role_CO3_P","role_1O2_P","role_3DOM_P","role_Phot_P","role_OH_I","role_CO3_I","role_1O2_I","role_3DOM_I","role_Phot_I","NO3_OH","NO2_OH","DOM_OH"');
  fprintf (fid, "\n");
  fclose(fid);

  for i = 1:length(x)*length(y),	% Compute the value for each point.
    % This means, compute all the values with fixed parameters and varying xx(i) and yy(i).

    if (iy > 0)
      ii(iy) = yy(i);
    endif;
    if (ix > 0)
      ii(ix) = xx(i);
    endif;
    [t_OH, t_CO3, t_1O2, t_3DOM, t_Phot, t_tot, k_OH, k_CO3, k_1O2, k_3DOM, k_Phot, k_tot, coOH, coCO3, co1O2, co3DOM, f_OH, f_CO3, f_1O2, f_3DOM, f_Phot, f_tot, y_tot, role_OH_P, role_CO3_P, role_1O2_P, role_3DOM_P, role_Phot_P, role_OH_I, role_CO3_I, role_1O2_I, role_3DOM_I, role_Phot_I, NO3_OH, NO2_OH, DOM_OH] = apex (file_prefix, ii(1), ii(2), ii(3), ii(4), ii(5), ii(6), ii(7), ii(8), ii(9), ii(10), ii(11), ii(12), ii(13), ii(14), ii(15), ii(16), ii(17), ii(18), ii(19), ii(20), ii(21));
    oo = [xx(i), yy(i), t_OH, t_CO3, t_1O2, t_3DOM, t_Phot, t_tot, k_OH, k_CO3, k_1O2, k_3DOM, k_Phot, k_tot, coOH, coCO3, co1O2, co3DOM, f_OH, f_CO3, f_1O2, f_3DOM, f_Phot, f_tot, y_tot, role_OH_P, role_CO3_P, role_1O2_P, role_3DOM_P, role_Phot_P, role_OH_I, role_CO3_I, role_1O2_I, role_3DOM_I, role_Phot_I, NO3_OH, NO2_OH, DOM_OH];
    csvwrite(filename, oo, "-append");
  end;

endfunction
