% Function to plot a graph for the APEX program.

% Copyright 2011, 2012 by Davide Vione and Marco Bodrato
% Licence CC BY-NC-SA

% This is part of the APEX program
% Aqueous Photochemistry of Environmentally occurring Xenobiotics

% To be edited to obtain different graphs.
% If a parameter (filemane) is given, it is used as a file name to save the graph
% in PDF format.
% Examples: plotgraph() returns the plot on the screen, plotgraph("graph3d.pdf") creates
% a PDF file with the 3D plot, in the same folder where plotgraph.m is placed.
% To obtain the PDF file in a different folder, type for instance *************

function plotgraph (filename)
% ===============================================================================================
% *** BEGINNING OF RANGE INPUT ***
	
  x=1:1:10;  % Range for the first variable, A:B:C means from A to C with steps of B.
  y=1:1:20;    % Range for the second variable.
	
% *** END OF RANGE INPUT ***
% ================================================================================================


% ================================================================================================
% *** INPUT FILE PREFIX ***
  % Here you should insert the name of the input file, which reports the spectra of the compound, 
  % sunlight and water, as well as the photolysis quantum yield. NOTE: the input file should be a .csv one.
  % file_prefix = prefix for filenames, named <file_prefix>"_LL.csv" ...

  file_prefix = "Test";
	
% *** END OF INPUT FILE PREFIX ***
% ================================================================================================

		
% ================================================================================================
% *** BEGINNING OF DATA INPUT ***
% NOTE: "-1" denotes the x variable, "-2" the y variable (see above for their ranges)

  % d = Water column depth (metres)
  d = -1;

  % CNO3 = Concentration of NO3- (molarity)
  CNO3 = 1e-4;

  % CNO2 = Concentration of NO2- (molarity)
  CNO2 = 1e-6;

  % NPOC = Dissolved Organic Carbon (DOC or NPOC, mgC/L or ppmC)
  NPOC = -2;

  % CCO3 = Concentration of CO3 2- (molarity)
  CCO3 = 1e-5;

  % CHCO3 = Concentration of HCO3- (molarity)
  CHCO3 = 1e-3;

  % CBr = Concentration of Br- (molarity)
  CBr = 1e-11;

  % kP_OH = Reaction rate between P and �OH (units of molarity^-1 seconds^-1, 0 if not available)
  kP_OH = 1e10;

  % kP_CO3 = Reaction rate between P and CO3-� (units of molarity^-1 seconds^-1, 0 if not available)
  kP_CO3 = 1;

  % kP_DOM = Reaction rate between P and CDOM triplet states, 3CDOM* (units of molarity^-1 seconds^-1, 0 if not available)
  kP_DOM = 4.5e7;

  % kP_1O2 = Reaction rate between P and singlet oxygen, 1O2 (units of molarity^-1 seconds^-1, 0 if not available)
  kP_1O2 = 6e4;

  % fi_P = direct photolysis quantum yield of P (unitless, 0 if not available) 
  fi_P = 0.33;

  % y_OH = yield of the intermediate via the �OH pathway (unitless, [formation rate of the intermediate via �OH]/[transformation rate of P])
  y_OH = 0.023;

  % y_CO3 = yield of the intermediate via the CO3-� pathway (unitless, [formation rate of the intermediate via CO3-�]/[transformation rate of P])
  y_CO3 = 0;

  % y_1O2 = yield of the intermediate via the 1O2 pathway (unitless, [formation rate of the intermediate via 1O2]/[transformation rate of P])
  y_1O2 = 0;

  % y_3DOM = yield of the intermediate via the 3CDOM* pathway (unitless, [formation rate of the intermediate via 3CDOM*]/[transformation rate of P])
  y_3DOM = 0.31;

  % y_Phot = yield of the intermediate via the direct photolysis(unitless, [formation rate of the intermediate via direct photolysis]/[transformation rate of P])
  y_Phot = 0.25;

% *** END OF DATA INPUT ***
% ================================================================================================


% ================================================================================================
% *** BEGINNING OF OUTPUT SELECTION ***

  % The function returns results as a vector in the following order
  %  1) t_OH  = The half-life time of P with .OH in Summer Sunny Days (SSD)
  %  2) t_CO3 = The half-life time of P with CO3-. in Summer Sunny Days (SSD)
  %  3) t_1O2 = The half-life time of P with 1O2 in Summer Sunny Days (SSD)
  %  4) t_3DOM = The half-life time of P with 3CDOM* in Summer Sunny Days (SSD)
  %  5) t_Phot = The half-life time of P by direct photolysis in Summer Sunny Days (SSD)
  %  6) t_tot = The overall half-life time of P in Summer Sunny Days (SSD)
  %  7) k_OH = The first-order rate constant of P for reaction with .OH (1/SSD)
  %  8) k_CO3 = The rate constant of P for reaction with CO3-. (1/SSD)
  %  9) k_1O2 = The rate constant of P for reaction with 1O2 (1/SSD)
  % 10) k_3DOM = The rate constant of P for reaction with 3CDOM* (1/SSD)
  % 11) k_Phot = The rate constant of P upon direct photolysis (1/SSD)
  % 12) k_tot = The overall rate constant for P degradation (1/SSD)
  % 13) coOH = The steady-state [.OH] in mol/L (22 w/m2 UV irradiance)
  % 14) coCO3 = The steady-state [CO3-.] in mol/L (22 w/m2 UV irradiance)
  % 15) co1O2 = The steady-state [1O2] in mol/L (22 w/m2 UV irradiance) 
  % 16) co3DOM = The steady-state [3CDOM*] in mol/L (22 w/m2 UV irradiance)
  % 17) f_OH = The first-order rate constant of intermediate formation upon reaction of P with .OH (1/SSD)
  % 18) f_CO3 = The rate constant of intermediate formation upon reaction of P with CO3-. (1/SSD)
  % 19) f_1O2 = The rate constant of intermediate formation upon reaction of P with 1O2 (1/SSD)
  % 20) f_3DOM = The rate constant of intermediate formation upon reaction of P with 3CDOM* (1/SSD)
  % 21) f_Phot = The rate constant of intermediate formation from P by direct photolysis (1/SSD)
  % 22) f_tot = The overall rate constant of intermediate formation (1/SSD)
  % 23) y_tot = Overall formation yield of the intermediate from P
  % 24) role_OH_P = Fraction of P transformation that is accounted for by .OH
  % 25) role_CO3_P = Fraction of P transformation that is accounted for by CO3-.
  % 26) role_1O2_P = Fraction of P transformation that is accounted for by 1O2
  % 27) role_3DOM_P = Fraction of P transformation that is accounted for by 3CDOM*
  % 28) role_Phot_P = Fraction of P transformation that is accounted for by direct photolysis.
  % 29) role_OH_I = Fraction of intermediate formation that is accounted for by .OH
  % 30) role_CO3_I = Fraction of intermediate formation that is accounted for by CO3-.
  % 31) role_1O2_I = Fraction of intermediate formation that is accounted for by 1O2.
  % 32) role_3DOM_I = Fraction of intermediate formation that is accounted for by 3CDOM*.
  % 33) role_Phot_I = Fraction of intermediate formation that is accounted for by direct photolysis.
  % 34) NO3_OH = Fraction of .OH formation accounted for by nitrate
  % 35) NO2_OH = Fraction of .OH formation accounted for by nitrite
  % 36) DOM_OH = Fraction of .OH formation accounted for by CDOM)

  % Select the variable to be plotted (es: var_to_plot = 6, means t_tot will be plotted).

  var_to_plot = 6;

% *** END OF OUTPUT SELECTION ***
% ================================================================================================


% ------------------------------------------------------------------------------------------------
% === INPUT OF QUANTUM YIELD VALUES FOR REACTIVE SPECIES PHOTOPRODUCTION BY CDOM (ONLY FOR EXPERT USERS!!!) ===
% NOTE: MODIFY THESE VALUES ONLY IF YOU DEFINITELY KNOW WHAT YOU ARE DOING!!!!

  qyieldOH_CDOM = 3e-5;
  carbonateyieldCO3_CDOM = 6.5e-3;	% NOTE: The relevant equation is: R_CO3_CDOM = carbonateyieldCO3_CDOM * CCO3 * PaCDOM
  qyield1O2_CDOM = 1.25e-3;
  qyieldTriplet_CDOM = 1.28e-3;

% === END OF INPUT OF QUANTUM YIELD VALUES
% ------------------------------------------------------------------------------------------------


%************************************************************************************************************
% ***** DO NOT TRY TO MODIFY THE FILE BELOW THIS POINT, UNLESS YOU ARE AN EXPERIENCED OCTAVE PROGRAMMER *****
%************************************************************************************************************




  ii = [d, CNO3, CNO2, NPOC, CCO3, CHCO3, CBr, kP_OH, kP_CO3, kP_DOM, kP_1O2, fi_P, y_OH, y_CO3, y_1O2, y_3DOM, y_Phot, qyieldOH_CDOM, carbonateyieldCO3_CDOM, qyield1O2_CDOM, qyieldTriplet_CDOM];
  ix = 1; iy = 1;
  for i = 1:length(ii),
    if (ii(i) == -1)
      ix = i;
    endif;
    if (ii(i) == -2)
      iy = i;
    endif;
  end;
  [xx,yy]=meshgrid(x,y);	% Prepare the grid for plotting.
  zz=xx;

  for i = 1:length(x)*length(y),	% Compute the value for each point.
	% This means, put in zz(i) the value computed with fixed parameters and varying xx(i) and yy(i).

    if (iy > 0)
      ii(iy) = yy(i);
    endif;
    if (ix > 0)
      ii(ix) = xx(i);
    endif;

    zz(i)=apexvec(file_prefix, ii(1), ii(2), ii(3), ii(4), ii(5), ii(6), ii(7), ii(8), ii(9), ii(10), ii(11), ii(12), ii(13), ii(14), ii(15), ii(16), ii(17), ii(18), ii(19), ii(20), ii(21))(var_to_plot);

    end;

    mesh(xx,yy,zz);		% Show the graph.

    if (nargin() > 0)	% If the filename was given, save the graph as a PDF file.
      print("-dpdf", filename);
    endif;
endfunction
